//
//  LocationMonitor.swift
//  Directory
//
//  Created by CtanLI on 2018-06-27.
//  Copyright © 2018 ctanLi. All rights reserved.
//

import CoreLocation

protocol UpdateLocationDelegate {
    func updateLocation()
}

class LocationMonitor: NSObject, CLLocationManagerDelegate {
    
    //
    // MARK: properties
    //
    
    // services
    static let SharedManager = LocationMonitor()
    var locationManager = CLLocationManager()
    var delegate: UpdateLocationDelegate?
    
    // data
    var currentLocation: CLLocation?
    
    
    //
    // MARK: creation
    //
    
    override init() {
        super.init()
        
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
            startMonitoring()
        }
    }
    
    
    //
    // MARK: implementations
    //
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            stopMonitoring()
            currentLocation = location
            self.delegate?.updateLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("Enter")
    }
    
    
    //
    // MARK: operations
    //
    
    func startMonitoring() {
        locationManager.startUpdatingLocation()
    }
    
    func stopMonitoring() {
        locationManager.stopUpdatingLocation()
    }
}


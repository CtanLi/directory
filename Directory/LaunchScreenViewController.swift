//
//  LaunchScreenViewController.swift
//  Directory
//
//  Created by CtanLI on 2018-06-28.
//  Copyright © 2018 ctanLi. All rights reserved.
//

import UIKit

class LaunchScreenViewController: UIViewController {

    static let ShowHomeScreen = "showHomeScreen"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.async(execute: {
            self.performSegue(withIdentifier: LaunchScreenViewController.ShowHomeScreen, sender: self)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

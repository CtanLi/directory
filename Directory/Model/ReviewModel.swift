//
//  ReviewModel.swift
//  Directory
//
//  Created by CtanLI on 2018-07-05.
//  Copyright © 2018 ctanLi. All rights reserved.
//

import Foundation

class ReviewModel {
    
    //
    // MARK:- properties
    //
    
    var name: String?
    var url: String?
    var time: String?
    var description: String?
    var imageURL: String?
    
    //
    // MARK:- creation
    //
    
    init(dictionary: NSDictionary) {
        time = dictionary["time_created"] as? String
        description = dictionary["text"] as? String
        url = dictionary["url"] as? String
       
        if let info = dictionary["user"] as? NSDictionary {
            name = info["name"] as? String
            if let imageURLString = info["image_url"] as? String {
                imageURL = imageURLString
            }
        }
    }
    
    class func reviews(array: [NSDictionary]) -> [ReviewModel] {
        var review = [ReviewModel]()
        for dictionary in array {
            let store = ReviewModel(dictionary: dictionary)
            review.append(store)
        }
        return review
    }
    
    //
    // MARK:- implementations
    //
    
    class func searchWithReviewParams(parameters: String?, completion: @escaping ([ReviewModel]?, String?) -> Void) {
        YelpApiManager.sharedInstance.getReviewWithId(parameters!, completion: completion)
    }
}

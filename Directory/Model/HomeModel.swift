//
//  HomeModel.swift
//  Directory
//
//  Created by CtanLI on 2018-06-27.
//  Copyright © 2018 ctanLi. All rights reserved.
//

import Foundation
import UIKit

struct HomeModel {
    
    var caption: String
    var image: UIImage
    
    init(caption: String, image: UIImage) {
        self.caption = caption
        self.image = image
    }
    
    init?(dictionary: [String: String]) {
        guard let caption = dictionary["Caption"], let photo = dictionary["Photo"],
            let image = UIImage(named: photo) else {
                return nil
        }
        self.init(caption: caption, image: image)
    }
    
    static func allData() -> [HomeModel] {
        var photos = [HomeModel]()
        guard let URL = Bundle.main.url(forResource: "StaticImage", withExtension: "plist"),
            let photosFromPlist = NSArray(contentsOf: URL) as? [[String:String]] else {
                return photos
        }
        for dictionary in photosFromPlist {
            if let photo = HomeModel(dictionary: dictionary) {
                photos.append(photo)
            }
        }
        return photos
    }
}

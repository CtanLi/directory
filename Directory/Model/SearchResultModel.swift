//
//  SearchResultModel.swift
//  Directory
//
//  Created by CtanLI on 2018-06-27.
//  Copyright © 2018 ctanLi. All rights reserved.
//

import Foundation

class SearchResultModel {
    
    //
    // MARK:- properties
    //
    
    var id: String?
    var url: String?
    var name: String?
    var city: String?
    var country: String?
    var state: String?
    var imageURL: String?
    var rating: NSNumber?
    var distance: String?
    var isClosed: Bool = false
    var reviewCount: NSNumber?
    var phone: String?
    var address1: String?
    var zip_code: String?
    var longitude: NSNumber?
    var latitude: NSNumber?
    var title: String?
    var price: String?
    
    //
    // MARK:- creation
    //
    
    init(dictionary: NSDictionary) {
        id = dictionary["id"] as? String
        name = dictionary["name"] as? String
        reviewCount = dictionary["review_count"] as? NSNumber
        url = dictionary["url"] as? String
        rating = dictionary["rating"] as? NSNumber
        phone = dictionary["display_phone"] as? String
        price = dictionary["price"] as? String
        //isClosed = dictionary["is_closed"] as! Bool
        
        if let location = dictionary["location"] as? NSDictionary {
            city = location["city"] as? String
            country = location["country"] as? String
            state = location["state"] as? String
            address1 = location["address1"] as? String
            zip_code = location["zip_code"] as? String
        }
        
        if let coordinate = dictionary["coordinates"] as? [String : AnyObject] {
            longitude = coordinate["longitude"] as? NSNumber
            latitude = coordinate["latitude"] as? NSNumber
        }
        
        if let categories = dictionary["categories"] as? [[String : AnyObject]] {
            for category in categories {
                title = category["title"] as? String
            }
        }
        
        let imageURLString = dictionary["image_url"] as! String
        imageURL = imageURLString
    }
    
    class func businesses(array: [NSDictionary]) -> [SearchResultModel] {
        var restaurants = [SearchResultModel]()
        for dictionary in array {
            let store = SearchResultModel(dictionary: dictionary)
            restaurants.append(store)
        }
        return restaurants
    }
    
    //
    // MARK:- implementations
    //
    
    class func searchWithRestaurantsParams(parameters: [String : AnyObject], completion: @escaping ([SearchResultModel]?, String?) -> Void) {
        YelpApiManager.sharedInstance.searchWithRestaurantsParams(parameters, completion: completion)
    }
}

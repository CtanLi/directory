//
//  SearchResultTableViewCell.swift
//  Directory
//
//  Created by CtanLI on 2018-06-27.
//  Copyright © 2018 ctanLi. All rights reserved.
//

import UIKit

protocol MapPinTappedDelegate {
    func mapPinTapped(index: IndexPath)
    func openebPage(index: IndexPath)
    func makeCall(index: IndexPath)
}

class SearchResultTableViewCell: UITableViewCell, Reusable {

    var delegate: MapPinTappedDelegate!
    var indexPath: IndexPath!
    var latitude: String = ""
    var longitude: String = ""
    var id: String?
    var phoneNumber: String?
    var photoURL: String?
    
    @IBOutlet weak var searchedImage: UIImageView!
    @IBOutlet weak var searchedName: UILabel!
    @IBOutlet weak var searchedAddress: UILabel!
    @IBOutlet weak var searchedPhoneNumber: UIButton!
    @IBOutlet weak var ratingView: CosmosView!{
        didSet {
            ratingView.settings.updateOnTouch = false
        }
    }
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var openWeb: UIButton!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var price: UILabel!
    
    var searchResultModel: SearchResultModel! {
        didSet {
            searchedName.text = searchResultModel.name
            searchedImage.loadImageUsingCacheWithURLString(urlString: searchResultModel.imageURL!)
            
            let address1 = ((searchResultModel.address1 ?? "") + ", " + (searchResultModel.city ?? ""))
            let address2 = ((searchResultModel.zip_code ?? "") + " " + (searchResultModel.country ?? ""))
            
            searchedAddress.text = (address1 + " " + address2)
            ratingView.rating = Double(truncating: searchResultModel.rating!)
            
            if let lat = searchResultModel.latitude {
                latitude = lat.stringValue
            }
            
            if let long = searchResultModel.longitude {
                longitude = long.stringValue
            }
            
            category.text = searchResultModel.title
            
            id = searchResultModel.id
            phoneNumber = searchResultModel.phone
            price.text = searchResultModel.price
            photoURL = searchResultModel.imageURL
        }
    }
    
    @IBAction func makeCall(_ sender: UIButton) {
        self.delegate?.makeCall(index: indexPath)
    }
    
    @IBAction func mapViewAction(_ sender: UIButton) {
        self.delegate?.mapPinTapped(index: indexPath)
    }
    
    @IBAction func openWeb(_ sender: UIButton) {
        self.delegate?.openebPage(index: indexPath)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

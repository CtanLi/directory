//
//  SearchResultViewController.swift
//  Directory
//
//  Created by CtanLI on 2018-06-27.
//  Copyright © 2018 ctanLi. All rights reserved.
//

import UIKit
import CoreLocation
import BRYXBanner

class SearchResultViewController: UIViewController, Reusable, UpdateLocationDelegate, MapPinTappedDelegate, ClosePanelViewDelegate {
   
    var searchValue: String?
    var id: String?
    var photoURL: String?
    var isLoaded = false
    var userLocationCordinate: CLLocationCoordinate2D?
    var searchedLocationCordinate: CLLocationCoordinate2D?
    private let location = LocationMonitor.SharedManager
    private var panelViewController: MapViewController?
    
    var searchResultModel = [SearchResultModel]() {
        didSet {
            searchResultTable.reloadData()
        }
    }
    
    @IBOutlet weak var mapPanelView: UIView!
    @IBOutlet weak var searchResultTable: UITableView!
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapPanelView.isHidden = true
        location.delegate = self
        searchResultTable.delegate = self
        searchResultTable.dataSource = self
        UIApplication.shared.statusBarView?.backgroundColor = UIColor(hexString: "1F2124")
        Loadingindicator.sharedInstance.showActivityIndicator()
        
        titleLabel.text = searchValue
        NotificationCenter.default.addObserver(forName: .flagsChanged, object: Network.reachability, queue: nil) { [weak self] (_) -> Void in
            self?.updateUserInterface()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        location.startMonitoring()
    }

    func updateLocation() {
        
        let userCurrentLocation = ("\(location.currentLocation?.coordinate.latitude ?? 0), \(location.currentLocation?.coordinate.longitude ?? 0)")
        
        if let coordinate = location.currentLocation?.coordinate {
            userLocationCordinate = coordinate
        }
        
        let parameters: [String : AnyObject] = ["term": searchValue as AnyObject, "location": userCurrentLocation as AnyObject]
        SearchResultModel.searchWithRestaurantsParams(parameters: parameters, completion: { (searchResultModel: [SearchResultModel]?, error: String?) -> Void in
            if searchResultModel == nil {
                Banner(title:"", subtitle: "LOCATION NOT FOUND...", image: nil, backgroundColor: BannerColors.red).show(duration: 2.0)
                self.navigationController?.popViewController(animated: true)
            } else if searchResultModel?.count == 0 {
                self.isLoaded = true
                
                let alert = UIAlertController(title: NSLocalizedString("Search not available.", comment: "Search not available."), message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { _ in
                    self.navigationController?.popViewController(animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                self.searchResultModel = searchResultModel!
                self.isLoaded = true
            }
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            Loadingindicator.sharedInstance.dismissActivityIndicatos()
        })
    }
    
    func updateUserInterface() {
        guard let status = Network.reachability?.status else { return }
        switch status {
        case .unreachable:
            Banner(title: "Ooops!", subtitle: "Network error check your connection.", image: nil, backgroundColor: BannerColors.red).show(duration: 2.0)
        case .wifi:
            Banner(title: "Network connected...", subtitle: "", image: nil, backgroundColor: BannerColors.green).show(duration: 2.0)
        case .wwan:
            Banner(title: "Connected...", subtitle: "Consider using wifi.", image: nil, backgroundColor: BannerColors.yellow).show(duration: 2.0)
        }
        print("Reachability Summary")
        print("Status:", status)
        print("HostName:", Network.reachability?.hostname ?? "nil")
        print("Reachable:", Network.reachability?.isReachable ?? "nil")
        print("Wifi:", Network.reachability?.isReachableViaWiFi ?? "nil")
    }
    
    func statusManager(_ notification: NSNotification) {
        updateUserInterface()
    }
 
    func mapPinTapped(index: IndexPath) {
        print("button tapped at index:\(index)")
        let cell = searchResultTable.cellForRow(at: index) as! SearchResultTableViewCell
        print("button tapped for searchedName:\(cell.searchedName.text ?? "")")
        
        if let lat = Double(cell.latitude), let long = Double(cell.longitude) {
            searchedLocationCordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        }
  
        mapPanelView.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.dismissButton.alpha = 0
        }) { (finished) in
            self.dismissButton.isHidden = finished
        }
        
        for childController in self.childViewControllers {
            if childController is MapViewController {
                panelViewController = childController as? MapViewController
                panelViewController?.delegate = self
                panelViewController?.locationCordinate = userLocationCordinate
                panelViewController?.searchedLocationCordinate = searchedLocationCordinate
                NotificationCenter.default.post(name: Notification.Name(GlobalConstants.Constants.locationUpdate), object: nil)
            }
        }
    }
    
    func openebPage(index: IndexPath) {
        let cell = searchResultTable.cellForRow(at: index) as! SearchResultTableViewCell
        id = cell.id
        photoURL = cell.photoURL
    
        let myStoryBoard = UIStoryboard(name: "Review", bundle: Bundle.main)
        let popup = myStoryBoard.instantiateViewController(withIdentifier: "ReviewViewController") as! ReviewViewController
        popup.id = id
        popup.photoURL = photoURL
        let navigationController = UINavigationController(rootViewController: popup)
        navigationController.navigationBar.isHidden = true
        navigationController.modalPresentationStyle = .overCurrentContext
        navigationController.modalTransitionStyle = .coverVertical
        self.present(navigationController, animated: true, completion: nil)
    }

    func closePanelView() {
        mapPanelView.isHidden = true
        self.dismissButton.alpha = 0
        self.dismissButton.isHidden = false
        UIView.animate(withDuration: 0.6) {
            self.dismissButton.alpha = 1
        }
    }
    
    func makeCall(index: IndexPath) {
        let cell = searchResultTable.cellForRow(at: index) as! SearchResultTableViewCell
        if let number = cell.phoneNumber {
            let result = String(number.filter { "01234567890.".contains($0) })
            if let url = URL(string: "tel://\(result)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    @IBAction func dismissScreen(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SearchResultViewController: UITableViewDelegate { }

extension SearchResultViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResultModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultTableViewCell.identifier, for: indexPath)
            as! SearchResultTableViewCell
        cell.searchResultModel = searchResultModel[indexPath.row]
        cell.delegate = self
        cell.indexPath = indexPath
        return cell
    }
}

//
//  ViewController.swift
//  Directory
//
//  Created by CtanLI on 2018-06-26.
//  Copyright © 2018 ctanLi. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, Reusable, UISearchBarDelegate {

    var data = HomeModel.allData()
    
    @IBOutlet weak var homeCollectionView: UICollectionView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchViewConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        searchBar.tintColor = .white
        searchBar.placeholder = "Search business"
        
        homeCollectionView.delegate = self
        homeCollectionView.dataSource = self
        
        UIApplication.shared.statusBarView?.backgroundColor = UIColor(hexString: "1F2124")
        view.backgroundColor = UIColor(hexString: "1F2124")
        
        if let _ = UIImage(named: "Pattern") {
            //view.backgroundColor = UIColor(patternImage: patternImage)
        }
        
        homeCollectionView?.backgroundColor = UIColor.clear
        homeCollectionView?.contentInset = UIEdgeInsets(top: 5, left: 10, bottom: 10, right: 10)
        
        // Set the PinterestLayout delegate
        if let layout = homeCollectionView?.collectionViewLayout as? CustomCollectionView {
            layout.delegate = self
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        
        let myStoryBoard = UIStoryboard(name: "SearchResult", bundle: Bundle.main)
        let searchResultViewController = myStoryBoard.instantiateViewController(withIdentifier: SearchResultViewController.identifier) as! SearchResultViewController
        searchResultViewController.searchValue = searchBar.text
        self.navigationController?.pushViewController(searchResultViewController, animated: true)
        
        searchBar.text = ""
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.endEditing(true)
    }
}

extension HomeViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath)
        if let annotateCell = cell as? HomeCollectionViewCell {
            annotateCell.homeData = data[indexPath.item]
        }
        return cell
    }
}

extension HomeViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! HomeCollectionViewCell
        let searchValue = cell.homeData?.caption
        
        let myStoryBoard = UIStoryboard(name: "SearchResult", bundle: Bundle.main)
        let searchResultViewController = myStoryBoard.instantiateViewController(withIdentifier: SearchResultViewController.identifier) as! SearchResultViewController
        searchResultViewController.searchValue = searchValue
        self.navigationController?.pushViewController(searchResultViewController, animated: true)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if(velocity.y > 0) {
            self.searchViewConstraintHeight.constant = 0
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
        } else {
            self.searchViewConstraintHeight.constant = 54
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
}

extension HomeViewController: HomeScreenLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        return data[indexPath.item].image.size.height
    }
}

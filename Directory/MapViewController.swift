//
//  MapViewController.swift
//  Directory
//
//  Created by CtanLI on 2018-06-28.
//  Copyright © 2018 ctanLi. All rights reserved.
//

import UIKit
import MapKit

protocol ClosePanelViewDelegate {
    func closePanelView()
}

class MapViewController: UIViewController, MKMapViewDelegate {
    
    var delegate: ClosePanelViewDelegate!
    private let location = LocationMonitor.SharedManager
    var locationCordinate: CLLocationCoordinate2D?
    var searchedLocationCordinate: CLLocationCoordinate2D?
    var annotation = MKPointAnnotation()
    var steps = [MKRouteStep]()
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var distanceConstraint: NSLayoutConstraint!
    @IBOutlet weak var distanceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        mapView.showsScale = true
        mapView.showsPointsOfInterest = true
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .followWithHeading
        NotificationCenter.default.addObserver(self, selector: #selector(self.routeLocation), name: NSNotification.Name(rawValue: GlobalConstants.Constants.locationUpdate), object: nil)
        self.distanceConstraint.constant = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func routeLocation() {
        let span = MKCoordinateSpanMake(0.05, 0.05)
        
        if let locationCordinate = locationCordinate {
            let region = MKCoordinateRegion(center: locationCordinate, span: span)
            self.mapView.setRegion(region, animated: true)
        }
        
        if let lat = searchedLocationCordinate?.latitude, let long = searchedLocationCordinate?.longitude {
            annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            mapView.addAnnotation(annotation)
            let userCoordinate = MKPlacemark(coordinate: locationCordinate!)
            let destinationCoordinate = MKPlacemark(coordinate: searchedLocationCordinate!)
            
            let sourceItem = MKMapItem(placemark: userCoordinate)
            let destinationItem = MKMapItem(placemark: destinationCoordinate)
            
            let directionRequest = MKDirectionsRequest()
            directionRequest.source = sourceItem
            directionRequest.destination = destinationItem
            directionRequest.transportType = .automobile
            
            let directions = MKDirections(request: directionRequest)
            directions.calculate { (response, error) in
                guard let response = response else {
                    if let _ = error {
                        print("ERROR")
                    }
                    return
                }
                
                let route = response.routes[0]
                self.mapView.removeOverlays(self.mapView.overlays)
                self.mapView.add(route.polyline, level: .aboveRoads)
                self.location.locationManager.monitoredRegions.forEach({ self.location.locationManager.stopMonitoring(for: $0)})
                
                self.steps = route.steps
                for i in 0 ..< route.steps.count {
                    let step = route.steps[i]
                    print(step.instructions)
                    print(step.distance)
                    let region = CLCircularRegion(center: step.polyline.coordinate, radius: 20, identifier: "\(i)")
                    self.location.locationManager.startMonitoring(for: region)
                    
                    let circle = MKCircle(center: region.center, radius: region.radius)
                    self.mapView.add(circle)
                }
                
                let rect = route.polyline.boundingMapRect
                self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
                
                let point1 = MKMapPointForCoordinate(self.locationCordinate!)
                let point2 = MKMapPointForCoordinate(self.searchedLocationCordinate!)
                let distanceInMeters = MKMetersBetweenMapPoints(point1, point2)
                let kilometers = distanceInMeters.inKilometers()
                let valueDistance = String.init(format: "%.3f", kilometers)
                
                UIView.animate(withDuration: 0.3, animations: {
                    self.distanceConstraint.constant = 37
                    self.view.layoutIfNeeded()
                    self.distanceLabel.text = "Total distance: \(valueDistance) km"
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                        self.distanceConstraint.constant = 0
                    })
                    
                }, completion: nil)
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.blue.withAlphaComponent(0.5)
            polylineRenderer.lineWidth = 5.0
            return polylineRenderer
        }
        
        if overlay is MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.strokeColor = .clear
            circleRenderer.fillColor = .clear
            circleRenderer.alpha = 0.5
            circleRenderer.lineWidth = 1.0
            return circleRenderer
        }
        return MKOverlayRenderer()
    }
    
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//
//        if annotation is MKUserLocation {
//            return nil
//        }
//
//        let reuseId = "pinIdentifier"
//
//        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
//        if pinView == nil {
//            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
//            pinView!.canShowCallout = true
//        }
//        else {
//            pinView!.annotation = annotation
//        }
//
//        //Adding Subtitle text
//        let subtitleView = UILabel()
//        //Decalare distanceInMeters as global variables so that you can show distance on subtitles
//        subtitleView.text = "jkfbgjwrgrwng" //\(distanceInMeters)"
//        pinView!.detailCalloutAccessoryView = subtitleView
//
//        return pinView
//    }
    
    @IBAction func dismissScreen(_ sender: UIButton) {
         self.delegate?.closePanelView()
        
        self.distanceConstraint.constant = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}



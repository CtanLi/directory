//
//  LoadingIndcator.swift
//  Directory
//
//  Created by CtanLI on 2018-06-28.
//  Copyright © 2018 ctanLi. All rights reserved.
//

import Foundation
import UIKit

class Loadingindicator {
    
    static let sharedInstance = Loadingindicator()
    
    var window = UIApplication.shared.keyWindow!
    var container: UIView!
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    
    func showActivityIndicator() {
        container = UIView()
        container?.frame =  CGRect(x:  UIScreen.main.bounds.size.width * 0.5 , y: UIScreen.main.bounds.size.width * 0.5, width: 100, height: 100)
        container?.center = window.center
        container.layer.cornerRadius = 6
        container.backgroundColor = UIColor(white: 0, alpha: 0.8)
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = CGPoint(x: container.frame.size.width / 2, y: container.frame.size.height / 2)
        
        container.addSubview(activityIndicator)
        window.addSubview(container)
        window.makeKeyAndVisible()
        
        activityIndicator.startAnimating()
    }
    
    func dismissActivityIndicatos() {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
    }
}

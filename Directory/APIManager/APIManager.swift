//
//  APIManager.swift
//  Directory
//
//  Created by CtanLI on 2018-06-27.
//  Copyright © 2018 ctanLi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class YelpApiManager: NSObject {
    
    static let sharedInstance = YelpApiManager()
    let kFetchReview = "/%@/reviews"
    
    //
    // MARK:- implementations
    //
    struct DirectoryServer {
        struct DirectoryMobileServer {
            static let baseURL = "https://api.yelp.com/v3/businesses"
        }
    }
    
    func urlFromPath(path: String) -> String {
        if (DirectoryServer.DirectoryMobileServer.baseURL == "") {
            return ""
        }
        let requestUrl = String(format: "%@%@", DirectoryServer.DirectoryMobileServer.baseURL, path)
        return requestUrl
    }
    
    // restaurants api call
    func searchWithRestaurantsParams(_ parameters: [String : AnyObject], completion: @escaping ([SearchResultModel]?, String?) -> Void) {
        getRestaurantsValues(parameters, completion: completion)
    }
    
    func getRestaurantsValues(_ parameters: [String : AnyObject], completion: @escaping ([SearchResultModel]?, String?) -> ()) {
        let httpHeaders: HTTPHeaders = ["Authorization": "Bearer \(GlobalConstants.Constants.apiKey)"]
        Alamofire.request(GlobalConstants.Constants.apiUrl, method: .get,
                          parameters: parameters,
                          encoding: URLEncoding.default,
                          headers: httpHeaders).responseJSON { (responseObject) in
                            if responseObject.result.isSuccess {
                                let response = JSON(responseObject.result.value ?? "")
                                //success(resJson)
                                let dictionaries = response["businesses"].arrayObject
                                if dictionaries != nil {
                                    completion(SearchResultModel.businesses(array: dictionaries as! [NSDictionary]), nil)
                                }
                                
                                if dictionaries == nil {
                                    if let error = response["error"].dictionaryObject {
                                        let code = error["code"] as! String
                                        print(code)
                                        completion(nil, code)
                                    }
                                }
                            }
                            
                            if responseObject.result.isFailure {
                                let error = responseObject.result.error! as NSError
                                print("Request failed with error: \(error.description)")
                            }
        }
    }
    
    // review api call
    func getReviewWithId(_ parameters: String, completion: @escaping ([ReviewModel]?, String?) -> Void) {
        getReviewValues(userID: parameters, completion: completion)
    }
    
    func getReviewValues(userID: String, completion: @escaping ([ReviewModel]?, String?) -> Void)  {
        let httpHeaders: HTTPHeaders = ["Authorization": "Bearer \(GlobalConstants.Constants.apiKey)"]
        let requestPath = urlFromPath(path: String(format: kFetchReview, userID))
        Alamofire.request(requestPath, method: .get, parameters: nil, encoding: URLEncoding.default, headers: httpHeaders).responseJSON { (responseObject) in
            if responseObject.result.isSuccess {
                let response = JSON(responseObject.result.value ?? "")
                print(response)
                let dictionaries = response["reviews"].arrayObject
                if dictionaries != nil {
                    completion(ReviewModel.reviews(array: dictionaries as! [NSDictionary]), nil)
                }
            } else {
                if responseObject.result.isFailure {
                    let error = responseObject.result.error! as NSError
                    print("Request failed with error: \(error.description)")
                }
            }
        }
    }
}

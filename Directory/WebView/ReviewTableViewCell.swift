//
//  ReviewTableViewCell.swift
//  Directory
//
//  Created by CtanLI on 2018-07-05.
//  Copyright © 2018 ctanLi. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell, UITextViewDelegate, Reusable {

   
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userDescription: UITextView! {
        didSet {
            userDescription.delegate = self
            userDescription.isScrollEnabled = false
            userDescription.isEditable = false
        }
    }
    @IBOutlet weak var timeStamp: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    var reviewModel: ReviewModel! {
        didSet {
            userName.text = reviewModel.name
            if let image = reviewModel.imageURL {
                userImage.loadImageUsingCacheWithURLString(urlString: image)
            }
            
            userDescription.text = reviewModel.description
            timeStamp.text = reviewModel.time
        }
    }
    
}

//
//  WebViewController.swift
//  Directory
//
//  Created by CtanLI on 2018-07-04.
//  Copyright © 2018 ctanLi. All rights reserved.
//

import UIKit
import WebKit

class ReviewViewController: PannableViewController, Reusable {

    var id: String?
    var photoURL: String?
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var reviewTable: UITableView!
    
    var reviewModel = [ReviewModel]() {
        didSet {
            reviewTable.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reviewTable.delegate = self
        reviewTable.dataSource = self
        reviewTable.estimatedRowHeight = reviewTable.rowHeight
        reviewTable.rowHeight = UITableViewAutomaticDimension
        
        photo.loadImageUsingCacheWithURLString(urlString: photoURL!)
        
        ReviewModel.searchWithReviewParams(parameters: id, completion: { (reviewModel: [ReviewModel]?, error: String?) -> Void in
            if reviewModel == nil {

            } else if reviewModel?.count == 0 {
                
                let alert = UIAlertController(title: NSLocalizedString("Search not available.", comment: "Search not available."), message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { _ in
                    self.navigationController?.popViewController(animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
            } else {
                self.reviewModel = reviewModel!
            }
        })

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func dismissScreen(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ReviewViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 116
//    }
}

extension ReviewViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviewModel.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReviewTableViewCell.identifier, for: indexPath)
            as! ReviewTableViewCell
        cell.reviewModel = reviewModel[indexPath.row]
        return cell
    }
}

